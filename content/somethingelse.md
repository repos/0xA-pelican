Title: Notes on using C64 1541-II floppy drive on Debian
Date: 2016-06-03 00:12
Category: Uncategorized
Status: draft

apt install -y tmux vim-nox  
echo -e 'APT::Install-Recommends "0";\\nAPT::Install-Suggests "0";' &gt;
/etc/apt/apt.conf.d/90norecommend  
apt install -y linux-headers-amd64 build-essential libusb-dev
ncurses-dev  
cd /usr/src  
wget
http://www.trikaliotis.net/Download/opencbm-0.4.99.98/opencbm-0.4.99.98.tar.bz2  
tar xvf opencbm-0.4.99.98.tar.bz2  
cd opencbm-0.4.99.98  
make -f LINUX/Makefile  
make -f LINUX/Makefile install-all  
ldconfig
