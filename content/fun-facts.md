Title: Fun Facts
Date: 2011-03-15 11:18
Status: published

Did you know?

\* 0xA is a mess.  
\* 0xA is version controlled, so it's OK.  
\* 0xA committers are using computers.  
\* 0xA has been around for a decade.  
\* 0xA rarely sounds the same twice (both bug and feature)  
\* 0xA means 11 in hexadecimal.  
\* 0xA has a Japanese launch model Mega Drive.  
\* 0xA told you so.
