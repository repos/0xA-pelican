Title: [THE_EXPR_METHOD] 009 Phase shifting
Date: 2016-04-07 17:52
Category: The [expr~] Method
Status: published

Previously, the fmod() function had been used in the context of
frequency multiplication. For example:

    [phasor~ 0.25] -> [expr~ fmod($v1*4, 1)]

Besides this, it can also be used to shift the phase position of a
sawtooth oscillator. The principle is largely the same as the example
above, apart from replacing the multiplication with addition, which
represents the amount of displacement. For example:

    [phasor~ 0.25] -> [expr~ fmod($v1+(1/4.0), 1)]

This will push the phase position of the input sawtooth by a quarter of
the total wavelength.

                                                 Original phase position
         .    /|       /|       /|       /|
         .   / |      / |      / |      / |
         .  /  |     /  |     /  |     /  |
         . /   |    /   |    /   |    /   |
         ./    |   /    |   /    |   /    |
         /     |  /     |  /     |  /     |
        /.     | /      | /      | /      |
       / .     |/       |/       |/       |
         .                                       Phase shift by 25%
         .  /|       /|       /|       /|
         . / |      / |      / |      / |
          /  |     /  |     /  |     /  |
         /   |    /   |    /   |    /   |
        /.   |   /    |   /    |   /    |
       / .   |  /     |  /     |  /     |
      /  .   | /      | /      | /      |
     /   .   |/       |/       |/       |

Musically speaking, it essentially adds a time delay/offset to the
original control signal. One way this can be useful is to arrange the
shifted signals in parallel, which would produce effects similar to a
musical canon. In practice, this enables the ability to generate
interesting compositional variations from a limited pre-defined
sequence. Furthermore, the amount of shifting can also be modulated just
the same way as amplitude and frequency, which allows for more dynamic
and responsive structures to emerge.

For controlling sound synthesis, phase shifting provides a simple
mechanism to move envelope and logic level along the time axis. Consider
the following:

    [phasor~ 0.25] -> [expr~ if($v1<0.25, 1, 0)] -> [s~ signal_1]
                  \-> [expr~ fmod($v1+0.25, 1)] -> [expr~ if($v1<0.25, 1, 0)] -> [s~ signal_2]

**signal\_1** produces a gate switching signal with 25% duty cycle. By
shifting the phase of the \[phasor\~\], **signal\_2** would have the
same rate of change, but delayed by quarter of the total wavelength.

It is worth mentioning that, attention needs to be paid to the total
amount of shift that are "allowed". that is to say, the "effective"
shift permitted would be the total wave period minus the duty cycle of
the gate. In the case of above example, if the amount of shift had been
greater than 0.75, the "on" portion of the gate would be pushed off the
right edge, and thus may cause undesired effect.

![009\_phase\_shift]({filename}images/009_phase_shift.png)

[Get the source here](https://gitlab.com/0xa/the_expr_method "source")

Exercise:

-   How to shift the phase of a sine wave?

