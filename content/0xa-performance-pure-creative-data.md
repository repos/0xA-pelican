Title: 0xA performance at Pure (creative) Data 2012
Date: 2012-03-25 19:33
Category: performances
Status: published

Chun Lee of 0xA will be giving a performance next Thursday in [Pure
(creative)
Data](http://www.stereolux.org/programmation/concert/performance/pure-creative-data-perfomrnaces-29-03-2012)festival
at [stereolux](http://www.stereolux.org/), Nante, France. The
performance will be based on the used of \[expr\~\] object in a
real-time live-coding context.
