Title: expr~ workshop in LAC 2013
Date: 2013-05-06 21:49
Category: workshop
Status: published

Chun Lee will be at the [Linux Audio
Conference](http://lac.linuxaudio.org/2013/ "Linux Audio Conference"),
giving a workshop on DSP based music composition methodology. See the
full schedule [here.](http://lac.linuxaudio.org/2013/program)
