Title: [THE_EXPR_METHOD] 003 sequencing
Date: 2015-12-20 14:26
Category: The [expr~] Method, Uncategorized
Status: published

Apart from the discrete changes in values, generating control signals
for sequencing, can be approached just the same way as that for
modulation. That is to say, one essentially find ways to "alter" the
appearance of a given waveform so that a "stepped" version of it emerges
as a result. Transforming a signal from continuously and smoothly
changing to being quantised and value-restricted is therefore the
objective when preparing control signals for sequencing.

The following quick example will produce a sawtooth signal that contains
10 distinct steps within its wave cycle.

    [phasor~ 1]->[expr~ floor($v1*10, 0)/10]

Because a quantised signal holds the same value temporarily until the
next element comes along, by using comparison operators ("equals for
example"), it becomes possible to isolate/select a certain portion of
the sequence. Since the comparative statements will return true for the
entire duration of given time "step", it thus allow for further
(differential) treatments to the signal that falls on either side of the
test. This property of the sequencing signal might initially seem
trivial, but it makes such a signal very useful in solving many of the
tasks that are commonly faced when dealing with time dependent
structures.

The following example demonstrates how one can select the last element
in the sequence of 4 incremental counts (0,1,2,3,0,1,2,3...), and
subsquently reassign its output value (0,1,2,0,0,1,2,0...).

    [phasor~ 1]->[expr~ floor($v1*4, 0)]->[expr~ if($v1 == 3, 0, $v1)]

Combining with relational tests, control signal for sequencing can
extend far beyond as being a simple counting mechanism. Many common
objects such as \[select\], \[spigot\], \[int\] and \[moses\] can all be
emulated easily. Dynamic signal routing and flexiable looping are other
examples that are relatively uncomplicated to implement.

The exmple below demonstrates an interesting way of counting by
utilising the modulo function.

![003\_modulo]({filename}images/003_modulo.png)

[Get the source here](https://gitlab.com/0xa/the_expr_method "source")

Exercise:

*What other arithmetic functions can be used to modify the control
signal?*

*Which other parts of the example patch can be modulated/sequenced?*
