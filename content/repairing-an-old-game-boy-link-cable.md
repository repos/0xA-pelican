Title: Repairing an old Game Boy Link Cable
Date: 2016-01-29 00:08
Category: Uncategorized
Status: published

Can't trade your pokémouille anymore?!

Fear not. Old GB link cables, specially third party, tend to eventually
suffer from contact failure. Connectors are usually OK, but the wires
not, which will give much headache if the link is used to transfer data
(hello corrupted saved banks, hello wasted last night).

The fix is easy though, cut the defective parts and solder back the...
possibly much shorter... new cable.

     ___________                       ___________ 
    |  6     2  |---------------------|  6     2  |
     \_5__3____/                       \_5__3____/ 
     Connector 1                       Connector 2

    Connector 1

    2   SOUT  Data Out        Black
    3   SIN   Data In         Blue
    5   SCK   Shift Clock     Yellow
    6   GND   Ground          Red

    Connector 2
    (may have data wires color reversed for optimised
    assembly line slavery)

    2   SOUT  Data Out        Blue (!)
    3   SIN   Data In         Black (!)
    5   SCK   Shift Clock     Yellow
    6   GND   Ground          Red

     SOUT ---------- SIN
     SIN  ---------- SOUT
     SCK  ---------- SCK
     GND  ---------- GND

Here is a dodgy 4 plugs GB/GBC link turned into a working 2 plugs GB
link.

![Game Boy Link Cable
fixed](http://0xa.kuri.mu/files/2016/01/game_boy_link_fixed.jpg){.alignnone
.size-full .wp-image-284 width="100%"}

![Game Boy Link Cable fixed]({filename}images/game_boy_link_fixed.jpg)
