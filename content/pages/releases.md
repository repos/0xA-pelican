Title: Releases
Date: 2014-06-23 22:12
Status: published

EXPR\~ (2011)
=============

![](http://ia600405.us.archive.org/0/items/GOSUB10-004/GOSUB10-004_400x400.png){.alignnone
width="100%"}

<http://zeroexa.bandcamp.com/album/expr>

[source
code](http://www.archive.org/download/GOSUB10-004/GOSUB10-004-src-0xA--expr_.tgz "source code")

Copyleft: This is a free work, you can copy, distribute, and modify it
under the terms of the [Free Art
License](http://artlibre.org/licence/lal/en/ "Free Art License").
