Title: [THE_EXPR_METHOD] 002 modulation
Date: 2015-12-11 22:52
Category: The [expr~] Method
Status: published

Modulation, can essentially be seen as applying the "shape" of one
signal onto another. For example, modulating the amplitude of an
oscillator (carrier signal) with an sawtooth wave (modulating/control
signal) would create a crescendo in loudness with a sudden drop in
volume at the end. Triangle wave, on the other hand would produce a
crescendo and followed by a smooth decrescendo. The resulting sound thus
follows the visual appearance of the modulating signal.

Visualising the control signal in such a manner can be quite a intuitive
process when applying modulations in composition.

Supposing the control signal always starts with a sawtooth wave
(\[phasor\~\]), the aim therefore is to find ways to "bend" its linear
ramp into other suitable/interesting shapes to be further made used of.
Following examples will demonstrate some simple transformations in which
a sawtooth wave is "shaped" into other useful waveforms.

-   Reverse Sawtooth:

    </p>
        [phasor~ 1]->[-~ 1]->[abs~]

-   Triangle wave:

    </p>
        [phasor~ 1]->[*~ 2]->[-~ 1]->[abs~]

-   Square/Pulse wave:

    </p>
        [phasor~ 1]->[expr~ if($v1<0.5, 1, 0)]

-   Attack/Decay envelope:

    </p>

    ![002\_mod]({filename}images/002_mod.png)

Above examples may seem trivial at first, they do however provide highly
functional shapes that can be very useful in many common compositional
scenarios.

Exercise:

*Use only \[expr\~\] object to reverse Sawtooth and Triangle wave*

*The Triangle wave above is not entirely correct, how can it be fixed?*

*How to do simple Pulse Width Modulation from the given example above?*
