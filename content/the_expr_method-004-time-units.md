Title: [THE_EXPR_METHOD] 004 Time units
Date: 2015-12-28 17:29
Category: The [expr~] Method
Status: published

Time units, here refers to the duration of one single musical event. It
is relative, in that a selected unit can be subdivided into smaller
parts, as well as summed up to form a larger section. That is to say, if
the time unit is measured in quarter notes, the sum of four units will
make up for one bar, and half of the unit would be an eighth note. If
the time unit were expressed as one bar, four of which together could
resolve to a musical phrase, and a semiquaver would be one sixteenth of
the unit duration.

To understand timing in this manner is extremely helpful when making
compositional arrangements using DSP control signals. As long as one is
able to express the required timing in terms of proportions relating the
chosen time units, simple arithmetic would be all it takes to implement
the solution.

For instance, if one period of a sawtooth wave is considered as one bar,
to produce the same waveform but at the rate of quarter notes would
simply be:

    [phasor~ 1]->[expr~ fmod($v1*4, 1)]->[send~ QTR_NOTES]
                 
               ->[send~ ONE_BAR]

This works by first multiplying the original signal with an desired
amount, in order to scale up the "rate of change" appropriately. It is
then passed through the fmod (Floating Point Modulo) function. By having
the second argument set to 1, fmod essentially discard the integer
portion of its input. This results to a signal which has the amplified
increment, but restricted to floating point values between 0 and 1.

To gain any other subdivisions of one bar at 1Hz, simply replace the
multiplication factor with another value. Thus, one can quickly produce
multiple and parallel timing signal from just one \[phasor\~\] object.

However, if one period of a sawtooth is decided to be one beat (quarter
notes, for example), to obtain the timing signal for one bar and beyond
would require a little more thought, but otherwise based on the same
principle as above.

    [phasor~ 0.25]->[expr~ fmod($v1*4,1)]->[send~ QTR_NOTES]
                    
                  ->[send ONE_BAR]

This works by first dividing the frequency of the starting \[phasor\~ \]
object, to reduce its rate by an appropriate amount (in this case, 4
times slower). The slowed down oscillator can then be used as the
"extended" timing. But, to regain the sawtooth of 1Hz as one beat, the
same fmod trick is again used.

In short, subdividing a given time unit does not change the frequency of
the original control signal. Whereas timing extension involves reducing
the frequency of the initial control oscillator.

![004\_fmod]({filename}images/004_fmod.png)

[Get the source here](https://gitlab.com/0xa/the_expr_method "source")

Exercise:

-   All the examples above measures time in Hz (1 period of 1Hz is 1
    second long), how could this be translated to use more conventional
    measurement such as BPM?

