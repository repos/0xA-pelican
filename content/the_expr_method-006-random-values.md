Title: [THE_EXPR_METHOD] 006  Random values
Date: 2016-01-28 21:41
Category: The [expr~] Method
Status: published

Having random values are often a quick and easy method to create
variations in compositional arrangements. Conventionally, the \[random\]
object is used for this purpose. By triggering the object with a bang,
it would output a new value within a specifiable range.

Random values can also be obtained using only audio control signals,
which involves the use of sample and hold function (\[samphold\~\] in
Pd). Generally speaking, it takes a snapshot of its input/source signal,
and output the sample value until it samples again. The second inlet of
\[samphold\~\], therefore, controls how often the new samples are taken.
In this case, it is when the signal to the second inlet has a negative
transition/increment.

For example, if a \[phasor\~\] is connected to the second input,
\[samphold\~\] will be triggered right at the boundary between each wave
cycle. This is because only at that point, the increment of \[phasor\~\]
would be negative. This unique property of sawtooth wave enables
\[samphold\~\] to take one-off samples at regular interval.

Once the principle of \[samphold\~\] is understood, simply connecting a
\[noise\~\] object to its first input would result to a randomized
signal. The random values would consequently change according to the
signal characteristics given to the second inlet. Simple numerical
manipulation might be necessary in order to scale the values to a
appropriate range thereafter.

![006\_samphold]({filename}images/006_samphold.png)

[Get the source here](https://gitlab.com/0xa/the_expr_method "source")

Exercise:

-   Besides using \[noise\~\] as a source for randomised signal, what
    other use might \[samphold\~\] also have?
-   What other wave forms might also be interesting to be used as the
    control signal for \[samphold\~\]?

