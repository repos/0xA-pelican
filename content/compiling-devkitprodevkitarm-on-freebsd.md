Title: Compiling devkitPro/devkitARM on FreeBSD
Date: 2014-06-25 15:49
Category: Uncategorized
Status: published

Yes it's possible to compile devkitPro/devkitARM on FreeBSD. It does
require a bit of tweaking though. Below are some notes I took while
compiling devkitARM release 42 and associated libraries on FreeBSD
10.0-STABLE. It's a lot of quick hacks, but that worked out for having a
fully working libga based environment.

    * bash script does not work
      -> replace /bin/bash with more portable /usr/bin/env bash
    * GNU MP is needed
      -> portmaster math/gmp
    * gmp header files not found while GCC is compiled
      -> problem files need manual editing pointing to the full path
      of the header, for instance /usr/local/include/gmp.h
    * linker cannot find -lmpc at the end of stage 1 GCC build
      -> -I/usr/local/include -L/usr/local/lib needs to be added
      in .devkitARM/arm-none-eabi/gcc/gcc/Makefile
    * abort defined twice in a row in libsysbase abort.c
      -> I removed the duplicate lines
    * same with iosupport.c ?
    * same with close.c, environ.c, execve.c, etc
      OK something is definitively wrong with the distribution of this
      source, or one of the scripts. I ended up `for ... do`-ing a lot
      of mv commands.
    * libfreeimage missing for building grit-0.8.12
      -> portmaster graphics/freeimage
    * grit configure cannot find libfreeimage
      -> configure/configure.ac modified to skip test
      and -I/usr/local/include -L/usr/local/lib
    * grit fails to compile because cannot find FreeImage.h
      -> relevant .h and .cpp files edited to 
      include /usr/local/include/FreeImage.h
    * tar tap error with libnds, libmirko, and maybe another one
      -> add '-f -' to relevant Makefile:
      bzip2 -cd libnds-$(VERSION).tar.bz2 |   
     tar -x -f - -C $(DEVKITPRO)/libnds
    * Maxmod fails to build
      ->replace @make with gmake in maxmod.mak
    * Examples installed manually
    * when compiling projects, clang is used, fails
      -> the Makefile needs editing to replace @make with gmake

It's very rough, but should give enough hints.

There is a small thread on the DevkitPro/DevkitARM forum on the matter:
<http://devkitpro.org/viewtopic.php?f=2&t=3203>
