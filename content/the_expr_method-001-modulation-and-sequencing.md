Title: [THE_EXPR_METHOD] 001 modulation and sequencing
Date: 2015-12-04 23:42
Category: The [expr~] Method
Status: published

Modulation and sequencing are two building blocks that are fundamental
to sound synthesis and music making. The former typically aims to
control the finer details and the timbre of a sound, whereas the latter
deals with the arrangement and the structure of "musical" events over
time. Their definitions can briefly be summarized as the following:

Modulation, applying a continuous signal onto a parameter, so the value
of the parameter changes *smoothly* over time.

Sequencing, applying a sequence of values onto a parameter, so the value
of the parameter changes *discretely* over time.

Modulation is generally easier to understand, as it often is taught as
the "introduction" to sound synthesis. The three properties of any given
oscillating sound (frequency, amplitude and phase) can all be modulated
to create interesting results.

Sequencing, on the other hand, is less common to achieve using digital
audio signal exclusively. The objective is essentially to quantize the
signal into discrete "steps". As a result, even though the signal is
continuous, the changes in its values are quantized accordingly. One can
also think of it as the reduction in "resolution" of a given signal. One
example would be to make a DSP version of a step counter.

The "trick" to make such a counter is simply to multiply the signal with
a (max count) variable, and then only to take the integer part of the
result. For example:

    [phasor~ 1]->[*~ 5]->[expr~ floor($v1, 0)]

This will result to a signal that counts from 0 to 4 in one second.

Having a good practical understanding on both modulation and sequencing
is crucial in grasping and taking full advantage of the topics to come.
The following questions can be used as starting points to approach the
subject.

*What are the differences and commonalities between modulation and
sequencing?  
*

*Are they essentially the same?  
*

*Where can they be applied?  
*  
EXAMPLE PATCH: 001\_mod-seq.pd

![001\_mod-seq]({filename}images/001_mod-seq.png)

[Get the source here](https://gitlab.com/0xa/the_expr_method "source")
