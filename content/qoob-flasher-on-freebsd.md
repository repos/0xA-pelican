Title: qoob-flasher on FreeBSD
Date: 2016-02-21 15:21
Category: Uncategorized
Status: published

![qoob-flasher on FreeBSD]({filename}images/qoob-flasher.jpeg)

Sometimes ago I dug up my old GameCube to give a try to
[GBI](http://www.gc-forever.com/forums/viewtopic.php?f=37&t=2782 "GBI"),
a very impressive homebrew alternative to the Game Boy Player Start-up
Disc.

To load homebrews, the GC had been modded 10 years ago by
[someone](http://pi.kuri.mu) with a now impossible to find [Qoob Chip
Pro](http://www.gc-forever.com/wiki/index.php?title=Qoob "Qoob Chip").
This great modchip was only provided with a Windows software to flash
it. However while looking for a copy of the program, I stumbled upon a
[really nice Linux CLI
version](https://gitorious.org/gqoob/mainline "gqoob") by [Joni
Valtanen](http://koti.kapsi.fi/jvaltane/). The software compiles and run
fine on Debian, but I also wanted to have it available on FreeBSD. As it
turns out, making it working for FreeBSD was fairly straightforward.
Crappy patches below.

The flashing software comes in two parts: libqoob and qoob-flasher. Only
libqoob sources need to be very slightly adjusted. FreeBSD comes with a
specific libusb implementation, so there is no need to check for the
regular one while configuring, but the libqoob pkg-config file needs to
be edited to point to the libusb shipped with the OS. Also, pkg-config
files are not located in a subdir of the lib directory on FreeBSD. All
together, some changes need to be made to Makefile.am, configure.ac and
libqoob.pc.in:

libqoob/Makefile.am:

    diff --git a/libqoob/Makefile.am b/libqoob/Makefile.am
    index 51cc260..7adacfb 100644
    --- a/libqoob/Makefile.am
    +++ b/libqoob/Makefile.am
    @@ -7,7 +7,11 @@ all-local: $(pcfiles)
     %.pc: %.pc
            cp $< $@
     
    +if FREEBSD
    +pkgconfigdir = /usr/local/libdata/pkgconfig
    +else
     pkgconfigdir = $(libdir)/pkgconfig
    +endif
     pkgconfig_DATA = $(pcfiles)
     
     deb-install:

libqoob/configure.ac:

    diff --git a/libqoob/configure.ac b/libqoob/configure.ac
    index 0ff95f2..13a0df2 100644
    --- a/libqoob/configure.ac
    +++ b/libqoob/configure.ac
    @@ -16,9 +16,18 @@ if test -z "$PKG_CONFIG" ; then
       AC_MSG_ERROR( pkg-config is required )
     fi
     
    -PKG_CHECK_MODULES(libusb, [libusb >= 0.1.12], , AC_MSG_ERROR( libusb >= 0.1-12 
    is required ))
    -AC_SUBST(libusb_CFLAGS)
    -AC_SUBST(libusb_LIBS)
    +if test "$(uname)" == "FreeBSD"; then
    +  PKG_CHECK_MODULES(libusb, [libusb-0.1 >= 0.1], , AC_MSG_ERROR( libusb >= 0.1 
    is required ))
    +  AC_SUBST(libusb_CFLAGS)
    +  AC_SUBST(libusb_LIBS)
    +  AC_SUBST(PACKAGE_REQUIRES, [libusb-0.1])
    +  AM_CONDITIONAL([FREEBSD], [true])
    +else
    +  PKG_CHECK_MODULES(libusb, [libusb >= 0.1.12], , AC_MSG_ERROR( libusb >= 0.1-12 is required ))
    +  AC_SUBST(libusb_CFLAGS)
    +  AC_SUBST(libusb_LIBS)
    +  AC_SUBST(PACKAGE_REQUIRES, [libusb])
    +fi
     
     dnl debug
     AC_ARG_ENABLE(debug,

libqoob/libqoob.pc.in:

    diff --git a/libqoob/libqoob.pc.in b/libqoob/libqoob.pc.in
    index 027e026..559e77c 100644
    --- a/libqoob/libqoob.pc.in
    +++ b/libqoob/libqoob.pc.in
    @@ -6,6 +6,6 @@ includedir=@includedir@/libqoob
     Name: libqoob
     Description: Qoob Pro flasher library
     Version: @VERSION@
    -Requires: libusb
    +Requires: @PACKAGE_REQUIRES@ 
     Libs: -L${libdir} -lqoob
     Cflags: -I${includedir}

Almost there. FreeBSD needs free() from its own memory allocation
functions:

    diff --git a/libqoob/src/qoob-defaults.h b/libqoob/src/qoob-defaults.h
    index aa17409..116f2f1 100644
    --- a/libqoob/src/qoob-defaults.h
    +++ b/libqoob/src/qoob-defaults.h
    @@ -20,6 +20,11 @@
     #ifndef _QOOB_DEFAULTS_H_
     #define _QOOB_DEFAULTS_H_
     
    +#if defined(__FreeBSD__)
    +#include <stdlib.h>
    +#include <malloc_np.h>
    +#endif
    +
     /* Common typedefs */
     typedef enum {
       QOOB_FALSE = 0,

Now libqoob will compile and install fine, and qoob-flasher will
configure, compile and install nicely out of the box on both Linux and
FreeBSD.

It's now time to plug your qoob'ed GC which will be claimed by the
generic USB HID module:

    ugen1.2: <QooB Team> at usbus1
    uhid0: <QooB Team QOOB Chip Pro, class 0/0, rev 1.10/1.00, addr 2> on usbus1

However unlike on Linux, qoob-flasher will not be bothered by that, so
there is a no need to blacklist the device! And now let's flash [some
homebrew](http://www.gc-forever.com/forums/viewtopic.php?t=1375 "SWISS")
:)
