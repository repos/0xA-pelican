Title: bitop videos
Date: 2011-10-09 23:05
Category: hacking
Status: published

http://www.youtube.com/watch?v=8EMDXTQxPUY

In the last couple of weeks there has been
[a](https://secure.flickr.com/photos/kylemcdonald/sets/72157627762378810/)
[lot](http://www.pouet.net/topic.php?which=8357)
[of](http://www.bemmu.com/music/index.html)
[nice](http://wurstcaptures.untergrund.net/music/)
[projects](http://entropedia.co.uk/generative_music/) triggered by some
C experiments from [viznut](http://countercomplex.blogspot.com/) and
friends.

The following videos are 0xA's contribution to this bitop chiptune
craze, a couple of audiovisual renditions of a few selected oneliners.
Unfortunately Youtube was not very happy when I tried to use the shiftop
codes as video titles, and it was refused even inside videos
description, so here is the detailed playlist with authors:

1.  [((t&gt;&gt;1%128)+20)\*3\*t&gt;&gt;14\*t&gt;&gt;18](http://www.youtube.com/watch?v=PhJgZk_aTLI) (harism)
2.  [t\*(((t&gt;&gt;9)&10)|((t&gt;&gt;11)&24)\^((t&gt;&gt;10)&15&(t&gt;&gt;15)))](http://www.youtube.com/watch?v=2VfovIH1aNQ) (tangent128)
3.  [t\*5&(t&gt;&gt;7)|t\*3&(t\*4&gt;&gt;10)](http://www.youtube.com/watch?v=2LpmSOZz7OU) (miiro)
4.  [((t\*(t&gt;&gt;8|t&gt;&gt;9)&46&t&gt;&gt;8))\^(t&t&gt;&gt;13|t&gt;&gt;6)](http://www.youtube.com/watch?v=aPse0LUSDW0) (xpansive)
5.  [(t\*(t&gt;&gt;5|t&gt;&gt;8))&gt;&gt;(t&gt;&gt;16)](http://www.youtube.com/watch?v=8EMDXTQxPUY) (tejeez)
6.  [((t\*("36364689"\[t&gt;&gt;13&7\]&15))/12&128)+(((((t&gt;&gt;12)\^(t&gt;&gt;12)-2)%11\*t)/4|t&gt;&gt;13)&127)](http://www.youtube.com/watch?v=SS2tttIWOG0) (ryg)
    **added 10/10**
7.  [(t\*t/256)&(t&gt;&gt;((t/1024)%16))\^t%64\*(0xC0D3DE4D69&gt;&gt;(t&gt;&gt;9&30)&t%32)\*t&gt;&gt;18](http://www.youtube.com/watch?v=bGMx2hlv-Pg) (ultrageranium)
    **added 12/10**

Now what was used to make the videos? Processing? Flash? Pd/PDP? Pd/GEM?
Max/MSP/Jitter? openFrameworks? vvvv? GLSL code? Yet another fancy hip
"creative" framework? Nope. Just Chuck Tes...

No. Just good old [mplayer](http://www.mplayerhq.hu). I swear. Mplayer,
the most underrated audiovisual 8bit synth ;)

I'm now cleaning a bit the shell script that I used to preview and
render the formulas above and I will post it here soonish.
