Title: DIY SD adapter for GameCube
Date: 2015-12-30 22:55
Category: hacking
Status: published

I needed to load some .dol files on my GameCube and was advised to get
one of these third party SD card readers for the console. After
searching a bit, I learned that the GC memory card system is in fact a
modified SD card reader and as a consequence it is possible to use an
existing memory card and [fit it with an SD card
socket](http://ngc.scorpei.com/GUIDE-SD_adapter.html "How to make your own Nintendo GameCube (and Wii) SD adapter").

I did not have such a socket but I thought I could directly use instead
a spare SD to Micro SD adapter and wire its pins directly on the pins
from the memory card pins that I would use as donor cart, given the
tutorial linked above also provided the equivalence table for the SD
card pins:

    SD Card  |  Memory Card
    ------------------------
        1    |      9
        2    |      5
        3    |    2 or 10
        4    |      4
        5    |      11
        6    |    2 or 10
        7    |      7

    + Memory Card Pins 1 and 12 needs to be wired

After that I came across [another
diagram](http://www.gc-forever.com/wiki/index.php?title=File:Sdgecko_wiring_diagram.png "DIY SD Gecko wiring diagram")
that suggested the addition of an activity LED for the data line between
Memory card pins 6 (VSS2/Earth) and 7 (Data Line 0).

I could not resist the complimentary additional rice.

![DIY SD Card reader GameCube]({filename}images/DIY_SD_Card_reader_GameCube.jpg)

Now I can load my .dol files, and I have more blinking LED stuff. What a
great double WIN evening it is.
