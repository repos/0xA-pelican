Title: Lullaby_in.c Part 1
Date: 2014-06-24 00:59
Category: Uncategorized
Status: published

Sooooo... 0xA is still alive I guess... :)

I'm currently working on a small GBA ROM for an upcoming net release
compilation that has the lullaby for theme. The idea is to make a small
software that will create lullaby-esque sequences using the 4 legacy DMG
sound channels of the GBA. That's a good excuse to get into DevkitARM,
although now I'm thinking that it might have been good to learn GBDK to
make a GB ROM instead. Ah well, can't be bothered to restart and maybe I
can use the opportunity to add some graphics.

So far the software is tuned for the Hungarian scale in C, which makes a
very dramatic lullaby somehow, I'll tune later on. It was quite
straightforward, the most delicate part being the sound registers, but
thanks to [belogic](http://belogic.com/gba/index.php) everything is
super well documented. In a nutshell, it's all about bitmasking and
feeding the right registers with 16bit words.

For instance a simple way to control the noise channel can be made like
this:

    int setSnd4(int cd, int sta, int stp, int ti, int re)
    {
        // DMG Channel 4 Noise parameters, reset and loop control
        // See specs: http://belogic.com/gba/registers.shtml#REG_SOUND4CNT_H
        //
        // cd    -- Clock divider frequency (0-7)
        // sta   -- Counter stages (0-1), 15 or 7
        // stp   -- Counter Pre-Stepper frequency (0-15)
        // ti    -- Timed mode (0-1), continuous or timed
        // re    -- Sound reset

        return (re << 15) | (ti << 14) | (stp << 4) | (sta << 3) | cd;
    }

    int playNote4(int noise)
    {  
        REG_SOUND4CNT_L=0xA100;
        REG_SOUND4CNT_H=noise;

        return 0;
    }

    playNote4(setSnd4(2,1,10,0,1));

That's just basic copypasta to get the idea. Once it is sequenced and
populated with random values, it sounds like this:

\[audio
ogg="http://0xa.kuri.mu/files/2014/06/lullaby\_noise\_test.ogg"\]\[/audio\]

Note: this is recorded from an emulator, too lazy to flash it. Yawn.
Sleep. Bye.
