Title: expr~ live coding in Paris 
Date: 2013-01-14 21:21
Category: performances
Status: published

Chun Lee will be giving a demonstration in Paris at [Gaite
Lyrique](http://www.gaite-lyrique.net/) on the 22nd of January. The talk
will focus on the use of the DSP expression object in music making,
especially in live coding context. Link to the event is
[here](http://www.gaite-lyrique.net/evenement/dev-art-live-code-expression).
