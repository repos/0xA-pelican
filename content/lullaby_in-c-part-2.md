Title: Lullaby_in.c Part 2
Date: 2014-07-08 00:03
Category: Uncategorized
Status: published

Making some progress. Next to the noise channel, the software is now
also using the three other legacy DMG sound generators (two square wave
generators and a 4-bit DAC wave generator).

As for the generative part, this lullaby relies on a 6/8 time signature
and only use dominant and tonic notes for the square parts. Every sound
generator loops through their respective six steps sequences, that are
populated pseudo randomly, following some basic constraints:

- Sound channel 1: randomly picking different octaves of the dominant
and tonic notes of the given scale, with some muting every now and then,
except on step 0 and 3, that are on all the time.

- Sound channel 2: same with a longer envelope and always muted on steps
1, 2, 4 and 5.

- Sound channel 3: whenever new sequences are generated, two WAV RAM
banks are filled with four chunks of random 4-bit crap (in fact I just
fill it entirely with 16-bit crap at once, that's more fancy), and the 6
steps are just randomly populated with 0s and 1s which then determine
which bank is played. As for the playback frequency, this is basically
one of notes from the given scale for the whole duration of the
sequence.

At the moment I'm still using the Hungarian minor scale, but starting to
think that if it would be nice to have few more available. It sounds
like this :

\[audio
ogg="http://0xa.kuri.mu/files/2014/07/lullaby\_in.c-20140707.ogg"\]\[/audio\]

(still from emulation at this point)
